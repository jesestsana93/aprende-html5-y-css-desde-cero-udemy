Curso de Udemy "Aprende HTML5 y CSS desde cero" por Juan Fernando Urrego
Duración: 6 horas

Configuración sublime text settings user:
{
	"color_scheme": "Packages/Color Scheme - Default/iPlastic.tmTheme",
	"font_size": 14,
	"highlight_modified_tabs":true,
	"ignored_packages":
	[
		"Vintage"
	],
	"open_files_in_new_window": false,
	"save_on_focus_lost": true,
	"tab_size":4
}

Bloques de comentarios automático:
comm-html-section + tab y comm-section seleccionandolo + tab para css